from icecream import ic

def add(x, y):
    return x + y

def myfunc(value):
    if value % 2 == 0:
        ic()
        return True
    else:
        return False

ic.disable()  # This is how you disable logging
ic.enable()   # This is how you enable logging

ic(myfunc(10))
ic(myfunc(11))
ic(myfunc(12))

def output_to_file(text):
    with open('debug_log.txt', 'a') as f:
        f.write(text + '\n')

ic.configureOutput(prefix='Debug| ', outputFunction=output_to_file,
                   includeContext=True)

ic(add(10, 20))